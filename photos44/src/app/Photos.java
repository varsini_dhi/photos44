
package app; 
import java.io.IOException;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import view.*;

/**
 * photos44 - program to make a photo library using java fx
 * Software Methodology - Assignment 4
 * @author Varsini Dhinakaran
 * @author Mehraaj Tawa
 * Last Commit on 4/13/2022
 */
public class Photos extends Application {    
	Stage mainStage; 
	/**
	 * @param primaryStage sets mainStage 
	 * @throws IOException catch any input/output exceptions that can occur
	 * @throws ClassNotFoundException catch any class not found exceptions that can occur
	 */
	public void start(Stage primaryStage) throws IOException, ClassNotFoundException  {   
		mainStage = primaryStage; 
		mainStage.setTitle("Photos");
		
		FXMLLoader loader = new FXMLLoader();   
		loader.setLocation(getClass().getResource("/view/LogIn.fxml"));
		Pane root = (Pane)loader.load();
		LoginController Controller = loader.getController();
		Controller.start();
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show(); 
		}
	/**
	 * @param args arguments for main function
	 */
	public static void main(String[] args) {
		launch(args);
	}
}