package view;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.*;

/**
 * controller class from edit photo screen
 * @author Varsini Dhinakaran
 * @author Mehraaj Tawa
 *
 */
public class EditPhotoController {

	private Stage stage;
	private Scene scene;
	private Parent root;
	
	 @FXML
	    private Button AddTag;

	    @FXML
	    private TextArea CurrentCaptionField;

	    @FXML
	    private TextArea TagField;

	    @FXML
	    private TextArea captionField;

	    @FXML
	    private ChoiceBox<String> choiceBox;

	    @FXML
	    private Button deleteTag;

	    @FXML
	    private ImageView imageView;

	    @FXML
	    private ListView<String> listTags;

	    @FXML
	    private TextArea newTagType;

    File selectedFile;
    String date;
    File check;
    List<String> tags = new ArrayList<String>();
    
    /**
     * function called once screen is switched
     * @param event identifies button click from edit photo button
     */
    public void enter(ActionEvent event){
    	choiceBox.setItems(AppDatabase.currentuser.getTagTypes());
    	photo g = AppDatabase.currentalbum.getPhotoByPath(AppDatabase.currentpath);
    	CurrentCaptionField.setText(g.getCaption());
    	captionField.setText(g.getCaption());
    	listTags.getItems().clear();
   		listTags.setItems(g.getTags());
   		tags = g.getTags();
    	
    }
    
    /**
     * returns to album view screen
     * @param event identifies button click from back button
     * @throws IOException identifies input/output exceptions
     */
    @FXML
    public void backToAV(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("AlbumView.fxml"));	
		root = loader.load();	
		albumViewController viewC = loader.getController();
		viewC.enter(event); 
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
    }
    
    /**
     * adds new photo to album object
     * @param event identifies button click from confirm button
     * @throws IOException identifies input/output exceptions
     * @throws ParseException identifies parse exceptions
     */
    public void confirmNewPic(ActionEvent event) throws IOException, ParseException {
    	photo g = AppDatabase.currentalbum.getPhotoByPath(AppDatabase.currentpath);
    	selectedFile = g.getFile();
    	
    	ObservableList<photo> allphotos = AppDatabase.currentuser.getListofPhotos();
    	photo photo;
    	photo tempPhoto = null; 
    	boolean alreadyMade = false;
    	for(photo p : allphotos){
    		if (p.getFile().equals(selectedFile)){
    			alreadyMade = true; 
    			tempPhoto = p;
    		}
    	}
    	
    	if(alreadyMade)
    	 photo = tempPhoto; 
    	else{
    		photo = new photo();
    	photo.setFile(selectedFile);
    	}
      	date = g.getDate();
    	photo.setDate(date);
    	photo.setCaption(captionField.getText());
    	photo.setpath(selectedFile.getAbsolutePath());
    	photo.clearTag();
    	for(String s: tags)
    	photo.addTag(s);
    	AppDatabase.save(); 
    	backToAV(event);
    }
    
    /**
     * adds a tag to the photo object
     * @param event identifies button click from add tag button
     */
    @FXML
    public void addTag(ActionEvent event) {
        ObservableList<String> tagsO = FXCollections.observableArrayList();
    	photo g = AppDatabase.currentalbum.getPhotoByPath(AppDatabase.currentpath);
        Stage mainStage = (Stage)((Node)event.getSource()).getScene().getWindow();
    	String type;
    	if(newTagType.getText().equals("Please Enter New Tag Type Here" )|| newTagType.getText().isBlank()){
    		type = choiceBox.getSelectionModel().getSelectedItem();
    	}
    	else {
    		type = newTagType.getText();
    		AppDatabase.currentuser.setTagTypes(type);
    	}
    	for( String s : tags){
    		if (s.equals(type + ": " + TagField.getText())){
    			Alert alert = new Alert(AlertType.ERROR);
    			alert.initOwner(mainStage);
    			alert.setTitle("Error");
    			alert.setHeaderText("Tag Already Applied");
    			alert.showAndWait();
    			return;
    		}
    	}
    	tagsO.clear();
		if(g.getTags().size() != 0) {
			for(int i = 0; i < g.getTags().size(); i++) {
				tagsO.add(g.getTags().get(i));
				System.out.println("in loop");
			}
		}
    	tags.add(type + ": " + TagField.getText());
        for(int i = 0; i < tags.size(); i++) {
        	tagsO.add(tags.get(i));
        }
		listTags.setItems(tagsO);
    }
    
    /**
     * deletes selected tag in list view from the photo object
     * @param event identifies button click from delete button
     */
    public void deleteTag(ActionEvent event){
    	ObservableList<String> tagsO = FXCollections.observableArrayList();
    	if(listTags.getSelectionModel().getSelectedItem() != null){
    		tags.remove(listTags.getSelectionModel().getSelectedItem());
    		tagsO.clear();
            for(int i = 0; i < tags.size(); i++) {
           	 tagsO.add(tags.get(i));
            }
    		listTags.setItems(tagsO);
    	}
    }
    

}
