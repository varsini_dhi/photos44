package view;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.*;

/**
 * controller class for log in screen
 * @author Varsini Dhinakaran
 * @author Mehraaj Tawa
 */
public class LoginController {

	private Stage stage;
	private Scene scene;
	private Parent root;
	
    @FXML
    private Button LogIn;

    @FXML
    private Label Title;

    @FXML
    private TextField username = new TextField();
    
    /**
     * function called once screen is switched
     * @throws ClassNotFoundException identifies class not found exceptions
     * @throws IOException identifies input/output exceptions
     */
    public void start() throws ClassNotFoundException, IOException{
    	AppDatabase.start(); 
    }
    
    /**
     * allows user to login and checks username
     * @param event identifies button click from log in button
     * @throws IOException identifies input/output exceptions
     */
    public void loginAttempt(ActionEvent event) throws IOException{
    	String InputUsername = username.getText().trim(); 
    	if(InputUsername.equals("admin")) {
    		changetoAdmin(event);
    		return;
    	}
    	if (InputUsername.isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(stage);
			alert.setTitle("Error");
			alert.setHeaderText("Username empty");
			alert.setContentText("Please enter a user name");
			alert.showAndWait();
			return;
		}
    	user user = AppDatabase.admin.getUser(InputUsername);
    	if (user != null) {
    		AppDatabase.currentuser = user;
    		changetoAlbumHome(event);
    		}
    	else{
    		Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(stage);
			alert.setTitle("Error");
			alert.setHeaderText("Invalid User");
			alert.setContentText("Please enter a valid Username");
			alert.showAndWait();
			return;
		}
    }
    
    /**
     * changes to admin view screen
     * @param event identifies button click from log in button
     * @throws IOException identifies input/output exceptions
     */
    public void changetoAdmin(ActionEvent event) throws IOException{
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("AdminView.fxml"));	
		root = loader.load();	
		AdminController adminC = loader.getController();
		adminC.populateListofUsers();
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
    }
    
    /**
     * changes to album home screen
     * @param event identifies button click from log in button
     * @throws IOException identifies input/output exceptions
     */
    public void changetoAlbumHome(ActionEvent event) throws IOException{
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("HomeView.fxml"));	
		root = loader.load();	
		AlbumHomeController homeC = loader.getController();	
		homeC.enter(event);
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
    }  
}
