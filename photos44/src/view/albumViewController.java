package view;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.Popup;
import javafx.stage.Stage;
import model.*;

/**
 * controller class album view screen 
 * @author Varsini Dhinakaran
 * @author Mehraaj Tawa
 */
public class albumViewController{

	private Stage stage;
	private Scene scene;
	private Parent root;
	
    @FXML
    private Button AVlogOut;
    
    @FXML
    private ImageView ImageView;

    @FXML
    private Button AVsearchPhotos;

    @FXML
    private Button AlbumViewBack;

    @FXML
    private Button MovePhoto;

    @FXML
    private Button addPhoto;

    @FXML
    private Label albumName;

    @FXML
    private Button copyPhoto;

    @FXML
    private SplitPane splitPane;
    
    @FXML
    private Button deletePhoto;

    @FXML
    private Button editPhoto;

    @FXML
    private TilePane tilePane;
    
    @FXML
    private ScrollPane sp;
    
    @FXML
    private Label noOfPhotos;
    
    @FXML
    private Label albumDateRange;
    
    int count = 0;
	int nRows = 0;
	int nCols = 8;
	int indexno = 0;
	
	private static final double ELEMENT_SIZE = 100;
	private static final double GAP = 20;

	/**
	 * function called when album view screen is loaded
	 * @param event identifies button click from open album button
	 * @throws IOException identifies input/output exceptions
	 */
	void enter(ActionEvent event) throws IOException{
		populateListofPhotos(event);
		albumName.setText(AppDatabase.currentalbum.getName());
		noOfPhotos.setText(AppDatabase.currentalbum.getNumberofphotos() + " Photos In Album");
		if(AppDatabase.currentalbum.getSmallestDate().equals("99/99/9999 99:99:99") || AppDatabase.currentalbum.getLargestDate().equals("00/00/0000 00:00:00")) {
			albumDateRange.setText(" ");
		}
		else {
			albumDateRange.setText(AppDatabase.currentalbum.getSmallestDate() + " - " + AppDatabase.currentalbum.getLargestDate());
		}
		tilePane.setPrefColumns(nCols);
        tilePane.setHgap(GAP);
        tilePane.setVgap(GAP); 
		AppDatabase.save();
	}
	
	/**
	 * adds photos to thumbnail view in album view screen 
	 * @param event
	 * @throws IOException identifies input/output exceptions
	 */
	void populateListofPhotos(ActionEvent event) throws IOException{	
		createElements(event, AppDatabase.currentalbum.getName());
		if ( AppDatabase.currentalbum.getSmallestDate().equals("99/99/9999 99:99:99") && (AppDatabase.currentalbum.getLargestDate().equals("00/00/0000 00:00:00"))){
			albumDateRange.setText("");
		}
		else {
			albumDateRange.setText(AppDatabase.currentalbum.getSmallestDate() + " - " + AppDatabase.currentalbum.getLargestDate());
		}
	}
	
	/**
	 * returns to home screen 
	 * @param event identifies button click from back button
	 * @throws IOException identifies input/output exceptions
	 */
    @FXML
    void BackHome(ActionEvent event) throws IOException {
    	AppDatabase.save();
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("HomeView.fxml"));	
		root = loader.load();	
		AlbumHomeController homeC= loader.getController();	
		homeC.enter(event);
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
    }

    /**
     * adds a photo photo to album object
     * @param event identifies button click from add photo button
     * @throws IOException identifies input/output exceptions
     */
    @FXML
    void add(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("NewPhoto.fxml"));	
		root = loader.load();	
		newPhotoController photoC = loader.getController();
		photoC.enter();	
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
    }

    /**
     * copies photo from one album object into another album object
     * @param event identifies button click from copy button
     * @throws IOException identifies input/output exceptions
     * @throws ParseException identifies parse exceptions
     */
    @FXML
    void copy(ActionEvent event) throws IOException, ParseException {	
    	Stage mainStage = (Stage)((Node)event.getSource()).getScene().getWindow();
    	photo currentphoto = AppDatabase.currentalbum.getPhotoByPath(AppDatabase.currentpath);
    	boolean gotPhoto = false;
    	if(!(currentphoto != null)) {
    		Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage);
			alert.setTitle("Error");
			alert.setHeaderText("No Photo Selected");
			alert.showAndWait();
			return;
    	}
    	
    	TextInputDialog dialog = new TextInputDialog();
    	dialog.setTitle("Copy Photo into Another Album");
    	dialog.setContentText("Enter name of album to move to: ");
    	Optional<String> result = dialog.showAndWait();
    	if (result.isPresent()) {
    		String albumcopyname = result.get().trim();
    		ObservableList<album> albums = AppDatabase.currentuser.getListofAlbums();
    		for(album b : albums) {
    			if(b.getName().equals(albumcopyname)) {
    				ObservableList<photo> photos = b.getListofPhotos(); 
    				for( photo p : photos){
    					if (p.equals(currentphoto)){
    						Alert alert = new Alert(AlertType.ERROR);
    	    				alert.initOwner(mainStage);
    	    				alert.setTitle("Error");
    	    				alert.setHeaderText("Photo already in Album ");
    	    				alert.showAndWait();
    	    				return;
    					}
    				}
    				AppDatabase.currentuser.getAlbum(albumcopyname).addPhoto(currentphoto);
    				AppDatabase.compareDate(albumcopyname, currentphoto.getDate());
					gotPhoto = true; 
					AppDatabase.save();
    			}
    		}
    			if(!gotPhoto){
    				Alert alert = new Alert(AlertType.ERROR);
    				alert.initOwner(mainStage);
    				alert.setTitle("Invalid Album Name");
    				alert.setHeaderText("Album Name not Found");
    				alert.setContentText("Please enter a different album name");
    				alert.showAndWait();
    				return;
    			}
    		}
    	}

    /**
     * delete's photo from album object
     * @param event identifies button click from delete button
     * @throws IOException identifies input/output exceptions
     * @throws ParseException identifies parse exceptions
     */
    @FXML
    void delete(ActionEvent event) throws IOException, ParseException {
    	Stage mainStage = (Stage)((Node)event.getSource()).getScene().getWindow();
    	photo currentphoto = AppDatabase.currentalbum.getPhotoByPath(AppDatabase.currentpath);
    	if(!(currentphoto != null)) {
    		Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage);
			alert.setTitle("Error");
			alert.setHeaderText("No Photo Selected");
			alert.showAndWait();
			return;
    	}
    	int count = 0; 
    	Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Action Required");
		alert.setHeaderText("Are you sure you would like to delete the selected photo? ");
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			ObservableList<album> albums = AppDatabase.currentuser.getListofAlbums(); 
			for( album b: albums){
				if(b.getPhotoByPath(AppDatabase.currentpath) != null && b.getPhotoByPath(AppDatabase.currentpath).equals(currentphoto))
					count++;
			}
			if(count == 1) {
				AppDatabase.currentuser.deletePhoto(currentphoto); 
			}
			AppDatabase.currentalbum.deletePhoto(currentphoto);
			if(AppDatabase.currentalbum.getListofPhotos().size() == 0) {
				AppDatabase.currentalbum.setLargestDate("00/00/0000 00:00:00");
				AppDatabase.currentalbum.setSmallestDate("99/99/9999 99:99:99");
			}
			else {
				AppDatabase.currentalbum.setLargestDate("00/00/0000 00:00:00");
				AppDatabase.currentalbum.setSmallestDate("99/99/9999 99:99:99");
				for(int i = 0; i < AppDatabase.currentalbum.getListofPhotos().size(); i++) {
					String date = AppDatabase.currentalbum.getListofPhotos().get(i).getDate();
					AppDatabase.compareDate(AppDatabase.currentalbum.getName(), date);
				}
			}
			ImageView.setImage(null);
		}
		populateListofPhotos(event);
    	AppDatabase.save();
    }
    
    /**
     * edit photo object's information
     * @param event identifies button click from edit photo button
     * @throws IOException identifies input/output exceptions
     */
    @FXML
    void edit(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("EditPhoto.fxml"));	
		root = loader.load();	
		EditPhotoController editC = loader.getController();
		editC.enter(event);
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
    }

    /**
     * returns to log in screen
     * @param event identifies button click from log out button
     * @throws IOException identifies input/output exceptions
     */
    @FXML
    void logout(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("LogIn.fxml"));	
		root = loader.load();	
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
    }

    /**
     * moves photo object from one album to another
     * @param event identifies button click from move button
     * @throws ParseException identifies  exceptions
     * @throws IOException identifies input/output exceptions
     */
    @FXML
    void move(ActionEvent event) throws ParseException, IOException {
    	Stage mainStage = (Stage)((Node)event.getSource()).getScene().getWindow();
    	photo currentphoto = AppDatabase.currentalbum.getPhotoByPath(AppDatabase.currentpath);
    	boolean gotPhoto = false;
    	if(!(currentphoto != null)) {
    		Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage);
			alert.setTitle("Error");
			alert.setHeaderText("No Photo Selected");
			alert.showAndWait();
			return;
    	}
    	TextInputDialog dialog = new TextInputDialog();
    	dialog.setTitle("Move Photo into Another Album");
    	dialog.setContentText("Enter name of album to move to: ");
    	Optional<String> result = dialog.showAndWait();
    	if (result.isPresent()) {
    		String albumcopyname = result.get().trim();
    		ObservableList<album> albums = AppDatabase.currentuser.getListofAlbums();
    		for(album b : albums) {
    			if(b.getName().equals(albumcopyname)) {
    				ObservableList<photo> photos = b.getListofPhotos(); 
    				for( photo p : photos){
    					if (p.equals(currentphoto)){
    						Alert alert = new Alert(AlertType.ERROR);
    	    				alert.initOwner(mainStage);
    	    				alert.setTitle("Error");
    	    				alert.setHeaderText("Photo already in Album ");
    	    				alert.showAndWait();
    	    				return;
    					}
    				}
    				AppDatabase.currentuser.getAlbum(albumcopyname).addPhoto(currentphoto);
    				AppDatabase.compareDate(albumcopyname, currentphoto.getDate());
					gotPhoto = true; 
					if(b.getPhotoByPath(AppDatabase.currentpath) != null && b.getPhotoByPath(AppDatabase.currentpath).equals(currentphoto))
						count++;					
					if(count == 1) {
						AppDatabase.currentuser.deletePhoto(currentphoto); 
					}
					AppDatabase.currentalbum.deletePhoto(currentphoto);
					ImageView.setImage(null);
					populateListofPhotos(event);
					AppDatabase.save();
    			}
    		}
    	}
    	if(!gotPhoto){
    		Alert alert = new Alert(AlertType.ERROR);
    		alert.initOwner(mainStage);
   			alert.setTitle("Invalid Album Name");
			alert.setHeaderText("Album Name not Found");
    		alert.setContentText("Please enter a different album name");
    		alert.showAndWait();
    		return;
    	}
   	}

    /**
     * switches to search photos screen
     * @param event
     * @throws IOException
     */
    @FXML
    void search(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("SearchView.fxml"));	
		root = loader.load();	
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
    }
	
    /**
     * populates tile pane view
     * @param event identifies button click from add photo button
     * @param albumName selects which album to populate tile pane with
     * @throws IOException identifies input/output exceptions
     */
	private void createElements(ActionEvent event, String albumName) throws IOException {
        tilePane.getChildren().clear();
        nRows = AppDatabase.currentuser.getAlbum(albumName).getNumberofphotos()/nCols + 1;
        count = 0;
        for (int i = 0; i < nRows; i++) {
            for (int j = 0; j < nCols; j++) {
                tilePane.getChildren().add(createPage(event, count, albumName));
                count++;
                if(count == AppDatabase.currentuser.getAlbum(albumName).getNumberofphotos()) {
                	return;
                }           
            }
        }
    }
	
	/**
	 * @param event identifies button click from add photo button
	 * @param index chooses which photo in the album is displayed
	 * @param albumName chooses which album is being displayed
	 * @return returns a vertical box to populate tile pane with photo and caption
	 * @throws IOException identifies input/output exceptions
	 */
	public VBox createPage(ActionEvent event, int index, String albumName) throws IOException {
		Stage parent = (Stage)((Node)event.getSource()).getScene().getWindow();
    	indexno = index;
        ImageView imageView = new ImageView();
        ImageView bigView = new ImageView();
        TextArea info = new TextArea();
        SplitPane split = new SplitPane();
        SplitPane topSplit = new SplitPane();
        Button left = new Button();
        Button right = new Button();
        Button close = new Button();
        VBox pageBox = new VBox();
        Label caption = new Label();
        Popup popup = new Popup();

        left.setText("<");
        left.setPrefWidth(ELEMENT_SIZE/2);
        left.setPrefHeight(ELEMENT_SIZE/2);
        right.setText(">");
        right.setPrefWidth(ELEMENT_SIZE/2);
        right.setPrefHeight(ELEMENT_SIZE/2);
        close.setText("close");
        close.setPrefWidth(ELEMENT_SIZE/2);
        close.setPrefHeight(ELEMENT_SIZE/2);
        info.setText(popInfoArea());
        
        split.setOrientation(Orientation.VERTICAL);
        imageView.setFitWidth(ELEMENT_SIZE);
        imageView.setFitHeight(ELEMENT_SIZE);
        imageView.setPickOnBounds(true);
        bigView.setFitWidth(ELEMENT_SIZE*5);
        bigView.setFitHeight(ELEMENT_SIZE*5);
        bigView.setPreserveRatio(true);
        imageView.setSmooth(true);
        imageView.setCache(true);
        
        ObservableList<photo> files = AppDatabase.currentalbum.getListofPhotos(); 
        if (files.size() == 0) return new VBox();
        File file = files.get(index).getFile();
        Image image = new Image(file.toURI().toString());
        imageView.setImage(image);
        AppDatabase.currentpath = file.getAbsolutePath();
        bigView.setImage(image);
        ImageView.setImage(image);
        
        split.getItems().addAll(topSplit, info, close);
        topSplit.getItems().addAll(left, bigView, right);
        
        EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>() { 
            @Override 
            public void handle(MouseEvent evt) { 
              	File file = files.get(index).getFile();
               	indexno = index;
               	Image image = new Image(file.toURI().toString());
                bigView.setImage(image);
                ImageView.setImage(image);
                AppDatabase.currentpath = file.getAbsolutePath();
                info.setText(popInfoArea());
              	popup.getContent().addAll(split);
               	popup.show(parent);
            } 
        };  
    
        caption.setText(files.get(index).getCaption());
        pageBox.getChildren().add(imageView);   
        pageBox.getChildren().add(caption);        

        EventHandler<MouseEvent> leftevt = new EventHandler<MouseEvent>() { 
            @Override 
            public void handle(MouseEvent leftevt) {
            	popup.getContent().clear();
            	indexno = indexno - 1;
            	if(indexno < 0 || indexno >= files.size()) {
              		indexno = indexno + 1;
              	}
            	File file = files.get(indexno).getFile();
                Image image = new Image(file.toURI().toString());
                
                bigView.setImage(image); 
                bigView.setFitWidth(ELEMENT_SIZE*5);
                bigView.setFitHeight(ELEMENT_SIZE*5);
                ImageView.setImage(image);
                
                AppDatabase.currentpath = file.getAbsolutePath();
                bigView.setPreserveRatio(true);
                topSplit.getItems().clear();
                split.getItems().clear();
                topSplit.getItems().addAll(left, bigView, right);
                split.getItems().addAll(topSplit, info, close);
                info.setText(popInfoArea());
            	popup.getContent().addAll(split);
            } 
         };  
         
         EventHandler<MouseEvent> rightevt = new EventHandler<MouseEvent>() { 
             @Override 
             public void handle(MouseEvent leftevt) {
              	popup.getContent().clear();
              	indexno = indexno + 1;
              	if(indexno >= files.size() || indexno < 0) {
              		indexno = indexno - 1;
              	}
              	File file = files.get(indexno).getFile();
                Image image = new Image(file.toURI().toString());
                
                bigView.setImage(image); 
                bigView.setFitWidth(ELEMENT_SIZE*5);
                bigView.setFitHeight(ELEMENT_SIZE*5);
                bigView.setPreserveRatio(true);
                
                ImageView.setImage(image);
                AppDatabase.currentpath = file.getAbsolutePath();
                info.setText(popInfoArea());
                topSplit.getItems().clear();
                split.getItems().clear();
                topSplit.getItems().addAll(left, bigView, right);
                split.getItems().addAll(topSplit, info, close);
             	popup.getContent().addAll(split);
             } 
          };  
          
          EventHandler<MouseEvent> closeHandler = new EventHandler<MouseEvent>() { 
              @Override 
              public void handle(MouseEvent evt) { 
              	popup.getContent().clear();
              	popup.hide();
              } 
           };  
           
           close.setOnMouseClicked(closeHandler);
           left.setOnMouseClicked(leftevt);
           right.setOnMouseClicked(rightevt);
           imageView.setOnMouseClicked(eventHandler);
           return pageBox;
    }  
	
	/**
	 * populates information of photo in slideshow text area
	 * @return returns a string to display in text area
	 */
	public String popInfoArea() {
		photo currentphoto = AppDatabase.currentalbum.getPhotoByPath(AppDatabase.currentpath);
    	String photoInfo = "";
        if (currentphoto != null) {
        	if(currentphoto.getCaption() != null && currentphoto.getDate() != null) {
        		photoInfo = currentphoto.getCaption() + "\n" + currentphoto.getDate() + "\n";
        	}
        	if(!currentphoto.getTags().isEmpty()) {
        		for(int i = 0; i < currentphoto.getTags().size(); i++) {
                	photoInfo = photoInfo + currentphoto.getTags().get(i) + "\n";
                }
        	}
        }
        return photoInfo;
	}
}
