package view;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;
import model.*;

/**
 * controller class for new photo screen
 * @author Varsini Dhinakaran
 * @author Mehraaj Tawa
 */
public class newPhotoController{

	private Stage stage;
	private Scene scene;
	private Parent root;
	
    @FXML
    private TextArea TagField;

    @FXML
    private TextArea captionField;

    @FXML
    private Button choosePhoto;
    
    @FXML
    private ChoiceBox<String> choiceBox = new ChoiceBox<String>();
    
    @FXML
    private ListView<String> listTags;

    @FXML
    private TextArea newTagType;
    
    @FXML
    private Pane pane;
    
    @FXML
    private Button AddTag;
    
    @FXML
    private ImageView imageView;
    
    File selectedFile;
    String date;
    File check;
    List<String> tags = new ArrayList<String>();
	
	/**
	 * 	switch back to album view screen
	 * @param event identifies button click from back button
	 * @throws IOException identifies input/output exceptions
	 */
    @FXML
    public void backToAV(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("AlbumView.fxml"));	
		root = loader.load();	
		albumViewController viewC = loader.getController();
		viewC.enter(event); 
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
    }
    
    /**
     * function called once screen is switched
     */
    public void enter() {
		choiceBox.setItems(AppDatabase.currentuser.getTagTypes());
    }
   
    /**
     * opens file chooser to add image
     * @param event identifies button click from choose picture button
     */
    @FXML
    public void choosePic(ActionEvent event) {
    	Stage parent = (Stage)((Node)event.getSource()).getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Photo");
        fileChooser.getExtensionFilters().add(new ExtensionFilter("Image Files", "*.bmp", "*.png", "*.jpg", "*.gif"));
        check = fileChooser.showOpenDialog(parent);
        if(check != null) {
        selectedFile = check;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        date = sdf.format(selectedFile.lastModified());
            Image image = new Image(selectedFile.toURI().toString());
            imageView.setFitWidth(125);
            imageView.setFitHeight(110);
            imageView.setSmooth(true);
            imageView.setCache(true);
            imageView.setImage(image);
        }   
        else
        	return; 
    }

    /**
     * adds new picture to photo array in album object with all related information
     * @param event identifies button click from confirm button
     * @throws IOException identifies input/output exceptions
     * @throws ParseException identifies parse exceptions
     */
    @FXML
    public void confirmNewPic(ActionEvent event) throws IOException, ParseException {
    	if(!(selectedFile != null)){
    		Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner((Stage)((Node)event.getSource()).getScene().getWindow());
			alert.setTitle("Error");
			alert.setHeaderText("No Photo Selected");
			//alert.setContentText("Please enter a different album name");
			alert.showAndWait();
			return;
    	}
    	ObservableList<photo> allphotos = AppDatabase.currentuser.getListofPhotos();
    	photo photo;
    	photo tempPhoto = null; 
    	boolean alreadyMade = false;
    	for(photo p : allphotos){
    		if (p.getFile().equals(selectedFile)){
    			alreadyMade = true; 
    			tempPhoto = p;
    		}
    	}
    	
    	if(alreadyMade)
    	 photo = tempPhoto; 
    	else{
    		photo = new photo();
    	photo.setFile(selectedFile);
    	}
    	
    	ObservableList<photo> albumphotolist = AppDatabase.currentalbum.getListofPhotos();
    	File newphoto = photo.getFile();
    	for(photo p : albumphotolist){
    		File loopphoto = p.getFile();
    		if (loopphoto.equals(newphoto)) {
    			Alert alert = new Alert(AlertType.ERROR);
    			alert.initOwner((Stage)((Node)event.getSource()).getScene().getWindow());
    			alert.setTitle("Error");
    			alert.setHeaderText("Photo Already In Album");
    			alert.setContentText("Please enter a different photo");
    			alert.showAndWait();
    			return;
    		}
    			
    	}
   
    	photo.setDate(date);
    	photo.setCaption(captionField.getText());
    	photo.setpath(selectedFile.getAbsolutePath());
    	photo.clearTag();
    	for(String s: tags)
    	photo.addTag(s);
    	AppDatabase.compareDate(AppDatabase.currentalbum.getName(), date);
    	AppDatabase.currentalbum.addPhoto(photo);

    	ObservableList<photo> photolist = AppDatabase.currentuser.getListofPhotos();
    	boolean dup = false;
    	for(photo p : photolist){
    		File loopphoto = p.getFile();
    		if (loopphoto.equals(newphoto))
    			dup = true;
    	}
    	if(!dup) {
    		AppDatabase.currentuser.AddPhoto(photo);
    	}
    	AppDatabase.currentpath = selectedFile.getAbsolutePath();
    	AppDatabase.save(); 
    	backToAV(event);
    }
    
    /**
     * adds tag to photo object
     * @param event identifies button click from add tag button
     */
    @FXML
    public void addTag(ActionEvent event) {
    	Stage mainStage = (Stage)((Node)event.getSource()).getScene().getWindow();
        ObservableList<String> tagsO = FXCollections.observableArrayList();
    	String type;
    	if(newTagType.getText().equals("Please Enter New Tag Type Here" )|| newTagType.getText().isBlank()){
    		type = choiceBox.getSelectionModel().getSelectedItem();
    	}
    	else {
    		type = newTagType.getText();
    		AppDatabase.currentuser.setTagTypes(type);
    	}
    	for( String s : tags){
    		if (s.equals(type + ": " + TagField.getText())){
    			Alert alert = new Alert(AlertType.ERROR);
    			alert.initOwner(mainStage);
    			alert.setTitle("Error");
    			alert.setHeaderText("Tag Already Applied");
    			alert.showAndWait();
    			return;
    		}
    	}
    	
    	tags.add(type + ": " + TagField.getText());
    	tagsO.clear();
        for(int i = 0; i < tags.size(); i++) {
       	 	tagsO.add(tags.get(i));
        }
		listTags.setItems(tagsO);
    	return;
    }

    /**
     * deletes selected tag in list view from photo object
     * @param event identifies button click from delete tag button
     */
    public void deleteTag(ActionEvent event){
    	ObservableList<String> tagsO = FXCollections.observableArrayList();
    	if(listTags.getSelectionModel().getSelectedItem() != null){
    		tags.remove(listTags.getSelectionModel().getSelectedItem());
    		tagsO.clear();
            for(int i = 0; i < tags.size(); i++) {
           	 tagsO.add(tags.get(i));
            }
    		listTags.setItems(tagsO);
    	}
    }

}
