package view; 

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.AppDatabase;
import model.album;
import model.photo;

/**
 * controller class for search view
 * @author Varsini Dhinakaran
 * @author Mehraaj Tawa
 */
public class SearchViewController {
	
	private Stage stage;
	private Scene scene;
	private Parent root;

    @FXML
    private Button back;

    @FXML
    private Button makeAlbum;

    @FXML
    private Button search;

    @FXML
    private TextField textArea;

    @FXML
    private TilePane tilePane;
    
    @FXML
    private CheckBox dateCheck;
    
    @FXML
    private CheckBox tagCheck;
    
	ArrayList<photo> results = new ArrayList<photo>();
    int count = 0;
	int nRows = 0;
	int nCols = 3;
	int indexno = 0;
	
	private static final double ELEMENT_SIZE = 75;
	private static final double GAP = 10;
	
	/**
	 * empty function
	 * @param evt identifies button click from search photos button
	 */
    public void enter(ActionEvent evt) {
    	
    }
    
    /**
     * switches to album view screen
     * @param event identifies button click from back button
     * @throws IOException identifies input/output exceptions
     */
    @FXML
    void backToPrev(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("AlbumView.fxml"));	
		root = loader.load();	
		albumViewController viewC = loader.getController();
		viewC.enter(event); 
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

    }

    /**
     * makes an album object from photo search results 
     * @param event identifies button click from make album from results button
     * @throws IOException identifies input/output exceptions
     * @throws ParseException identifies parse exceptions
     */
    @FXML
    void makeAlbum(ActionEvent event) throws IOException, ParseException {
    	boolean duplicate = false;
    	Stage mainStage = (Stage)((Node)event.getSource()).getScene().getWindow();
    	TextInputDialog dialog = new TextInputDialog();
    	dialog.setTitle("Create New Album");
    	dialog.setContentText("Enter name: ");
    	Optional<String> result = dialog.showAndWait();
    	if (result.isPresent()) {
    		String name = result.get().trim();
    		ObservableList<album> albums = AppDatabase.currentuser.getListofAlbums();
    		for(album b : albums) {
    			if(b.getName().equals(name)) {
    				duplicate = true; 
    				Alert alert = new Alert(AlertType.ERROR);
    				alert.initOwner(mainStage);
    				alert.setTitle("Invalid Album Name");
    				alert.setHeaderText("Album Name already in use");
    				alert.setContentText("Please enter a different album name");
    				alert.showAndWait();
    			}
    	}
    		if (duplicate == false)
    			AppDatabase.currentuser.AddAlbum(name);
    			AppDatabase.currentuser.getAlbum(name).populatePhotoList(results);
    			for(int i= 0; i < results.size(); i++) {
    				AppDatabase.compareDate(name, results.get(i).getDate());
    			}
    		
    	}
    	AppDatabase.save();    	
    }
    
    /**
     * searches all of user's photos based on tag value or date range
     * @param event identifies button click from search button
     * @throws ParseException identifies parse exceptions
     */
    @FXML
    void search(ActionEvent event) throws ParseException {
    	if(dateCheck.isSelected() && tagCheck.isSelected()) {
    		Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner((Stage)((Node)event.getSource()).getScene().getWindow());
			alert.setTitle("Error");
			alert.setHeaderText("Both Tag and Date Filters Cannot be Selected");
			alert.showAndWait();
			return;
    	}
    	else if(dateCheck.isSelected()) {
    		dateSearch(textArea.getText());
    	}
    	else if(tagCheck.isSelected()) {
    		tagSearch(textArea.getText());
    	}
    	else {
    		Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner((Stage)((Node)event.getSource()).getScene().getWindow());
			alert.setTitle("Error");
			alert.setHeaderText("No Filter Type Selected");
			alert.showAndWait();
			return;
    	}
    	createElements();
    }
    
    /**
     * searches all photo objects for photo with matching date range
     * @param dateRange date range that is used to find photos
     * @throws ParseException identifies parse exceptions
     */
    public void dateSearch(String dateRange) throws ParseException {
    	results.clear();
    	String[] dates = dateRange.split(" - ");
    	ArrayList<photo> all = AppDatabase.currentuser.getAllPhotos();
    	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		dates[0] = dates[0] + " 00:00:00";
		dates[1] = dates[1] + " 00:00:00";
		Date smallest = sdf.parse(dates[0]);
		Date biggest = sdf.parse(dates[1]);
    	for(int i = 0; i < AppDatabase.currentuser.getAllPhotos().size(); i++) {
    		Date photo = sdf.parse(all.get(i).getDate());
    		if (smallest.compareTo(photo) < 0 && biggest.compareTo(photo) > 0) {
    			results.add(all.get(i));
    		}
    	}
    }
    
    /**
     * searches all photos for photo objects that have tag pair
     * @param tagPair tag pair that is used to find photos
     */
    public void tagSearch(String tagPair) {
    	results.clear();
    	tagPair.toLowerCase();
    	String[] tags = tagPair.split(" ");
    	String tag1, tag2;
    	ArrayList<photo> all = AppDatabase.currentuser.getAllPhotos();
    	ObservableList<String> allTags = FXCollections.observableArrayList(); 
    	boolean one = false;
    	boolean two = false;
    	if(tags.length == 2) {
        	tag1 = tags[0] + " " + tags[1];
        	tag1.toLowerCase();
    		for(int i = 0; i < AppDatabase.currentuser.getAllPhotos().size(); i++) {
        		allTags = all.get(i).getTags();
        		for(int j = 0; j < allTags.size(); j++) {
        			if(allTags.get(j).equals(tag1)) {
        				results.add(all.get(i));
        			}
        		}
        	}
    	}
    	else {
    		for(int i = 0; i < AppDatabase.currentuser.getAllPhotos().size(); i++) {
        		tag1 = tags[0] + " " + tags[1];
            	tag2 = tags[3] + " " + tags[4];
            	tag1.toLowerCase();
            	tag2.toLowerCase();
        		allTags = all.get(i).getTags();
        		for(int j = 0; j < allTags.size(); j++) {
        			if(tags[2].equals("and")) {
        				if(tag1.equals(allTags.get(j))) {
        					one = true;
        				}
        				if(tag2.equals(allTags.get(j))) {
        					two = true;
        				}
        				if(one && two) {
        					results.add(all.get(i));
        					one = false;
        					two = false;
        				}
        			}
        			if(tags[2].equals("or")) {
        				if(tag1.equals(allTags.get(j))) {
        					one = true;
        				}
        				if(tag2.equals(allTags.get(j))) {
        					two = true;
        				}
        				if(one || two) {
        					results.add(all.get(i));
        					one = false;
        					two = false;
        					break;
        				}
        			}
        		}
        	}
    	}
    }
    
    /**
     * populates tile pane with search results
     */
    private void createElements() {
    	tilePane.setHgap(GAP);
        tilePane.setVgap(GAP); 
        tilePane.getChildren().clear();
        nRows = results.size()/nCols + 1;
        count = 0;
        for (int i = 0; i < nRows; i++) {
            for (int j = 0; j < nCols; j++) {
                tilePane.getChildren().add(createPage(count, results));
                count++;
                if(count == results.size()) {
                	return;
                }
                        
            }
        }
    }
	
    /**
     * populates tile pane with search results
     * @param index index of photo object in photo array 
     * @param results array list of photo search results
     * @return returns vbox to populate tile pane with image and caption
     */
	public VBox createPage(int index, ArrayList<photo> results) {
    	
    	indexno = index;        
        ImageView imageView = new ImageView();
        
        ObservableList<photo> files = FXCollections.observableArrayList();
        for(int i = 0; i< results.size(); i++) {
        	files.add(results.get(i));
        }
        if (files.size() == 0) 
        	return new VBox();
        File file = files.get(index).getFile();
        Image image = new Image(file.toURI().toString());
        imageView.setImage(image);
        imageView.setFitWidth(ELEMENT_SIZE);
        imageView.setFitHeight(ELEMENT_SIZE);
        imageView.setPickOnBounds(true);
        imageView.setSmooth(true);
        imageView.setCache(true);
        VBox pageBox = new VBox();
        Label caption = new Label();
        caption.setText(files.get(index).getCaption());
        pageBox.getChildren().add(imageView);   
        pageBox.getChildren().add(caption);        
      
        return pageBox;
    } 
}
