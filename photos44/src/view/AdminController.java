package view;

import java.io.IOException;
import java.util.Optional;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import model.*;
/**
 * controller class for admin screen
 * @author Varsini Dhinakaran
 * @author Mehraaj Tawa
 */
public class AdminController {

	private Stage stage;
	private Scene scene;
	private Parent root;

    @FXML
    private ListView<String> ListofUsers = new ListView<String>();

    @FXML
    private Button addUser;

    @FXML
    private Button deleteUser;

    @FXML
    private Button logOut;

    @FXML
    private TextField textField;

    /**
     * function that populates admin screen list view with all current users
     */
    public void populateListofUsers(){
    	ListofUsers.getItems().clear();
    	ObservableList<user> users = AppDatabase.admin.getListofUsers();
    	for(int i = 0; i < AppDatabase.admin.getNumberofUsers(); i++){
    		ListofUsers.getItems().add(users.get(i).getName());
    	}    	
    }
    
    /**
     * adds a new user
     * @param event identifies button click
     * @throws IOException identifies input/output exceptions
     */
    public void addUser(ActionEvent event) throws IOException{
    	String newUserName = textField.getText().trim();
    	Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Action Required");
		alert.setHeaderText("Are you sure you would like to add a user with the follwing name: " + newUserName + " ?");
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			ObservableList<user> listofusers = AppDatabase.admin.getListofUsers();
			for(user u : listofusers){
				if (u.getName().equals(newUserName)){
					Alert alert2 = new Alert(AlertType.ERROR);
	    			alert2.initOwner((Stage)((Node)event.getSource()).getScene().getWindow());
	    			alert2.setTitle("Error");
	    			alert2.setHeaderText("User Name already in use");
	    			alert2.setContentText("Please enter a different user name");
	    			alert2.showAndWait();
	    			return;
				}
			}
			AppDatabase.admin.addUser(newUserName);
			populateListofUsers();
			AppDatabase.save(); 
		}
    }
    
    /**
     * switches back to log in screen
     * @param event identifies log out button click
     * @throws IOException identifies input/output exceptions
     */
    public void logOut(ActionEvent event) throws IOException{
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("LogIn.fxml"));	
		root = loader.load();	
		LoginController logInC= loader.getController();	
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
    }
    
    /**
     * deletes selected user from list view
     * @param event identifies delete user button click
     * @throws IOException identifies input/output exceptions
     */
    public void deleteUser(ActionEvent event) throws IOException{
    	String username = ListofUsers.getSelectionModel().getSelectedItem();
    	user user = AppDatabase.admin.getUser(username);
    	Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Action Required");
		alert.setHeaderText("Are you sure you would like to delete user with the follwing name: " + username + " ?");
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
    	AppDatabase.admin.deleteUser(user);
    	populateListofUsers();
    	AppDatabase.save();
		}
    }
}
