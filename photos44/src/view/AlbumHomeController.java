package view;

import java.io.IOException;
import java.util.Optional;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;
import model.*;

/**
 * controller class for album home screen
 * @author Varsini Dhinakaran
 * @author Mehraaj Tawa
 */
public class AlbumHomeController {

	private Stage stage;
	private Scene scene;
	private Parent root;
	
    @FXML
    private Button AddAlbum;

    @FXML
    private ListView<String> AlbumList = new ListView<String>();

    @FXML
    private Pane AlbumPhotos;

    @FXML
    private TilePane TilePane;
    
    @FXML
    private Button DeleteAlbum;

    @FXML
    private Button LogOut;

    @FXML
    private Button OpenAlbum;

    @FXML
    private Button RenameAlbum;
    
    @FXML
    private SplitPane SplitPane;
    
    int count = 0;
	int nRows = 0;
	int nCols = 3;
	int indexno = 0;

	/**
	 * function to be called once the home screen is loaded --> populates list view with album names and info and selects first item
	 * @param event identifies button click of log in
	 * @throws IOException identifies input/output exceptions
	 */
    public void enter(ActionEvent event) throws IOException {
    	populateListofAlbums();
    	AlbumList.getSelectionModel().select(0); // selects first item
    }
    
    /**
     * sets list view of home screen with album names, no of photos and date range of album
     * @throws IOException identifies input/output exceptions
     */
    public void populateListofAlbums() throws IOException{
    	AlbumList.getItems().clear();
    	ObservableList<album> albums = AppDatabase.currentuser.getListofAlbums();
    	
    	for(int i = 0; i < AppDatabase.currentuser.getNumberofAlbums(); i++){
    		if(albums.get(i).getNumberofphotos() == 0){
    			AlbumList.getItems().add(albums.get(i).getName() + " | " + albums.get(i).getNumberofphotos() + " Photos, No Date Range");
    		}
    		else if ( albums.get(i).getSmallestDate().equals("99/99/9999 99:99:99") && (!albums.get(i).getLargestDate().equals("00/00/0000 00:00:00"))){
    			AlbumList.getItems().add(albums.get(i).getName() + " | " + albums.get(i).getNumberofphotos() + " Photos, From "
        				+ albums.get(i).getLargestDate() + " to " + albums.get(i).getLargestDate());
    		}
    		else if ( (!albums.get(i).getSmallestDate().equals("99/99/9999 99:99:99")) && albums.get(i).getLargestDate().equals("00/00/0000 00:00:00")){
    			AlbumList.getItems().add(albums.get(i).getName() + " | " + albums.get(i).getNumberofphotos() + " Photos, From "
        				+ albums.get(i).getSmallestDate() + " to " + albums.get(i).getSmallestDate());
    		}
    		else if (albums.get(i).getSmallestDate().equals("99/99/9999 99:99:99") && albums.get(i).getLargestDate().equals("00/00/0000 00:00:00")){
    			AlbumList.getItems().add(albums.get(i).getName() + " | " + albums.get(i).getNumberofphotos() + " Photos");
    		}
    		else {
    		AlbumList.getItems().add(albums.get(i).getName() + " | " + albums.get(i).getNumberofphotos() + " Number of Photos, From "
    				+ albums.get(i).getSmallestDate() + " to " + albums.get(i).getLargestDate());
    		}
    		ObservableList<photo> allphotos = AppDatabase.currentuser.getListofPhotos();
    		if(allphotos.size() != 0){
    			for(photo p : allphotos){
    				System.out.println(p.getCaption());
    			}
    		}
    	}
    	AppDatabase.save();
    }
    
    /**
     * add new album
     * @param event identifies button click from add album button
     * @throws IOException identifies input/output exceptions
     */
    public void addAlbum(ActionEvent event) throws IOException {
    	boolean duplicate = false; 
    	Stage mainStage = (Stage)((Node)event.getSource()).getScene().getWindow();
    	TextInputDialog dialog = new TextInputDialog();
    	dialog.setTitle("Create New Album");
    	dialog.setContentText("Enter name: ");
    	Optional<String> result = dialog.showAndWait();
    	if (result.isPresent()) {
    		String name = result.get().trim();
    		ObservableList<album> albums = AppDatabase.currentuser.getListofAlbums();
    		for(album b : albums) {
    			if(b.getName().equals(name)) {
    				duplicate = true; 
    				Alert alert = new Alert(AlertType.ERROR);
    				alert.initOwner(mainStage);
    				alert.setTitle("Invalid Album Name");
    				alert.setHeaderText("Album Name already in use");
    				alert.setContentText("Please enter a different album name");
    				alert.showAndWait();
    			}
    		
    	}
    		if (duplicate == false)
    			AppDatabase.currentuser.AddAlbum(name); 
    		
    	}
    	populateListofAlbums();
    	AppDatabase.save(); 
    }
    
    /**
     * deletes selected album from list view
     * @param event identifies button click from delete album button
     * @throws IOException identifies input/output exceptions
     */
    public void deleteAlbum(ActionEvent event) throws IOException {
    	Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Action Required");
		alert.setHeaderText("Are you sure you would like to delete the selected album? ");
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			String albumlistname = AlbumList.getSelectionModel().getSelectedItem();
			int index = albumlistname.indexOf("|");
			String albumname = albumlistname.substring(0,index).trim();
			System.out.println(albumname);
			
			ObservableList<photo> deletedPhotos = AppDatabase.currentuser.getAlbum(albumname).getListofPhotos();
			AppDatabase.currentuser.DeleteAlbum(albumname);
			
			ObservableList<album> newAlbumList = AppDatabase.currentuser.getListofAlbums();
			
			for(photo p : deletedPhotos) {  //for a single photo
				boolean duplicate = false; 
				for(album b : newAlbumList) {  //Go through all other albums
					for(photo pic : b.getListofPhotos()) {  //and compare to their photos, if dup --> true 
						if(pic.equals(p)) {
						duplicate = true; 
						}
					}		
				}
				if(!duplicate)		// If there is no duplicate (as in picture is unique), then delete picture 
					AppDatabase.currentuser.deletePhoto(p);
			}
        	populateListofAlbums();
        	AppDatabase.save(); 
        }
    }
    
    /**
     * renames album object
     * @param event identifies button click from rename album button
     * @throws IOException identifies input/output exceptions
     */
    public void renameAlbum(ActionEvent event) throws IOException {
    	String albumlistname = AlbumList.getSelectionModel().getSelectedItem().trim();
		int index = albumlistname.indexOf("|");
		String oldName = albumlistname.substring(0,index).trim();
    	TextInputDialog dialog = new TextInputDialog();
    	dialog.setTitle("Rename Album");
    	dialog.setContentText("Enter new name: ");
    	Optional<String> result = dialog.showAndWait();
    	if (result.isPresent()) {String name = result.get(); AppDatabase.currentuser.renameAlbum(oldName, name); }
    	populateListofAlbums();
    	AppDatabase.save(); 
    }
    
    /**
     * changes to album view screen
     * @param event identifies button click from open album button
     * @throws IOException identifies input/output exceptions
     */
    public void openAlbum(ActionEvent event) throws IOException{
    	String albumlistname = AlbumList.getSelectionModel().getSelectedItem();
		int index = albumlistname.indexOf("|");
		String albumname = albumlistname.substring(0,index).trim();
		album album = AppDatabase.currentuser.getAlbum(albumname);
		AppDatabase.currentalbum = album;
		FXMLLoader loader = new FXMLLoader(getClass().getResource("AlbumView.fxml"));	
		root = loader.load();	
		albumViewController viewC = loader.getController();
		
		viewC.enter(event); 
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
    }
    
    /**
     * changes to log in screen
     * @param event identifies button click from log out button
     * @throws IOException identifies input/output exceptions
     */
    public void changetoLogIn(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("LogIn.fxml"));	
		root = loader.load();	
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
    }
}


