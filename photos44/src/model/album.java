package model;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * album class to store list of photos, album date range and album name
 * @author Varsini Dhinakaran
 * @author Mehraaj Tawa
 */
public class album implements Serializable{
	/**
	 *  list of all photo objects in album
	 */
	private List<photo> photos = new ArrayList<photo>();
	/**
	 *  name of the album
	 */
	protected String title; 
	/**
	 *  date of album
	 */
	protected String date; 
	/**
	 *  date of earliest photo
	 */
	private String smallestDate; 
	/**
	 *  date of latest photo
	 */
	private String LargestDate;
	
	/**
	 * constructor also creates stock album with 5 default photos
	 * @param albumname create new album with albumname as it's name field
	 */
	public album (String albumname) {
		title = albumname.trim(); 
		if(albumname.equals("Stock")){
			photo s1 = new photo(1);
			photo s2 = new photo(2);
			photo s3 = new photo(3);
			photo s4 = new photo(4);
			photo s5 = new photo(5);
			photos.add(s1);
			photos.add(s2);
			photos.add(s3);
			photos.add(s4);
			photos.add(s5);
		}
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");  
	    date = formatter.format(currentDate); 
	    this.smallestDate = "99/99/9999 99:99:99";
	    this.LargestDate = "00/00/0000 00:00:00";
	}
	/**
	 * populates photo array list
	 * @param photo copies photo into photo array list of album
	 */
	public void populatePhotoList(ArrayList<photo> photo){
		this.photos = photo;
	}
	
	/**
	 * returns name of album
	 * @return returns name of the album
	 */
	public String getName(){
		return this.title; 
	}
	
	/**
	 * get image from file object
	 * @param picture file passed to determine which photo to return
	 * @return returns photo object based on if the parameter picture matches the photo object's file
	 */
	public photo getPhoto(File picture){
		for(photo b : photos){
			if ( picture.equals(b.getFile()))
				return b;
		}
		return null; 
	}
	
	/**
	 * gets image using path
	 * @param path string path to identify which photo object to return
	 * @return returns photo object that has a file with a path equal to parameter path
	 */
	public photo getPhotoByPath(String path){
		for(photo b : photos){
			if ( path != null && path.equals(b.getpath()))
				return b;
		}
		return null; 
	}
	
	/**
	 * returns photo array size
	 * @return returns size of photo array list
	 */
	public int getNumberofphotos(){
		return photos.size();
	}
	
	/**
	 * returns observable list of photos
	 * @return returns observable list of photos that has been converted from photo array list
	 */
	public ObservableList<photo> getListofPhotos(){
		ObservableList<photo> listofphotos = FXCollections.observableArrayList();
		for(int i = 0; i < photos.size(); i++){
    		listofphotos.add(photos.get(i));
    	}
		return listofphotos; 
	} 
	
	/**
	 * adds photo to photo array
	 * @param photo add photo object into photo array list of album
	 */
	public void addPhoto(photo photo){
		photos.add(photo);
	}
	
	/**
	 * deletes photo from photo array
	 * @param photo deletes photo object from photo array list of album
	 */
	public void deletePhoto(photo photo){
		photos.remove(photo);
	}
	
	/**
	 * returns earliest photo date
	 * @return returns string of the smallest date of a photo in the album
	 */
	public String getSmallestDate() {
		return this.smallestDate;
	}
	
	/**
	 * returns latest photo date
	 * @return returns string of the largest date of a photo in the album
	 */
	public String getLargestDate() {
		return this.LargestDate;
	}
	
	/**
	 * sets earliest photo date
	 * @param date sets string date as the smallest date of a photo in the album
	 */
	public void setSmallestDate(String date) {
		this.smallestDate = date;
	}
	
	/**
	 * sets latest photo date
	 * @param date sets string date as the largest date of a photo in the album
	 */
	public void setLargestDate(String date) {
		this.LargestDate = date;
	}
	
}