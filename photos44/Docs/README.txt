***VERY IMPORTANT***
FOR THE TA: When I (Varsini Dhinakaran) set up my Atlassian account, I signed up using google but that doesn't create a password for Atlassian. Therefore, i can't log myself in through powershell when cloning a repository to verify my identity and connect my Atlassian account. Therefore, I created an access key for the repository for my computer so all of my commits and pushes and anything I do to the work show up as an "unknown" user. Please note that this unknown user is me.  

Notes:

 - We left album name and user name to be case sensitive because we did not see any instructions regarding case sensitivity 

 - Serialization should be working properly, but in the case that it does not, please go to file AppDatabase.java, within the start method, comment out "admin = readApp();" and un comment "//admin = new admin()"

- search photos function is contained in the album view screen
